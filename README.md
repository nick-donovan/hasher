# RustHasherLib

RustHasherLib or just "hasher" is a simple library for personal projects that computes checksum hashes for files.

## Usage
- Before using the library in your project make sure to add the dependency in your `Cargo.toml` file.
    ```toml
    [dependencies]
    hasher = { git = "https://gitlab.com/nick-donovan/hasher.git" }
    ```
- Afterward, it can be used like so:
    ```rust
    use std::path::PathBuf;
    use hasher::{Hasher, hasher_error::HasherError};
    
    fn hash_file(file_path: &str) -> Result<String, HasherError> {
        let buffer_size: usize = 512;
        let mut hasher = Hasher::build("sha256", buffer_size)?;
    
        let file = PathBuf::from(file_path);
        Ok(hasher.hash_file(&file)?)
    }
    ```

## Supported Hashes
- md5
- sha1
- sha256
- sha512
- blake2b
- blake2s

## Acknowledgments

This project uses the following repository for calculating checksums:

- [RustCrypto](https://github.com/RustCrypto/hashes) ([Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0) and [MIT License](https://opensource.org/license/mit/))

### Copying
RustHasherLib is licensed under the GPL version 3 or later. See the [COPYING](https://www.gnu.org/licenses/gpl-3.0.txt) file for more information.