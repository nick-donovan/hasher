/*
    Copyright (c) 2023 Nick Donovan

    This file is part of hasher.

    hasher is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
    License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
    version.

    hasher is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
    details.

    You should have received a copy of the GNU General Public License along with hasher. If not, see
    <https://www.gnu.org/licenses/>.
 */

pub mod hasher_error;

use std::fs::File;
use std::io::{BufReader, Read};
use std::path::PathBuf;

use digest::DynDigest;
use crate::hasher_error::HasherError;


pub struct Hasher {
    hasher: Box<dyn DynDigest>,
    buff_size: usize,
}


impl Hasher {
    pub fn build(algorithm: &str, buff_size: usize) -> Result<Hasher, HasherError> {
        match algorithm.to_lowercase().as_str() {
            "md5" => Ok(Hasher { hasher: Box::new(md5::Md5::default()), buff_size }),
            "sha1" => Ok(Hasher { hasher: Box::new(sha1::Sha1::default()), buff_size }),
            "sha256" => Ok(Hasher { hasher: Box::new(sha2::Sha256::default()), buff_size }),
            "sha512" => Ok(Hasher { hasher: Box::new(sha2::Sha512::default()), buff_size }),
            "b2b" | "b2" | "blake2" => Ok(Hasher { hasher: Box::new(blake2::Blake2b512::default()), buff_size }),
            "b2s" => Ok(Hasher { hasher: Box::new(blake2::Blake2s256::default()), buff_size }),
            _ => Err(HasherError(format!("Invalid hash algorithm: {}", algorithm))),
        }
    }

    pub fn hash_file(&mut self, file_path: &PathBuf) -> Result<String, HasherError> {
        let mut reader = BufReader::new(File::open(file_path)?);
        let mut buffer = vec![0; self.buff_size];

        loop {
            let bytes_read = reader.read(&mut buffer)?;
            if bytes_read == 0 {
                break;
            }
            self.update(&buffer[..bytes_read]);
        }

        let hash_vec = self.finalize();
        Ok(format_hash(hash_vec))
    }

    fn update(&mut self, data: &[u8]) {
        self.hasher.update(data);
    }

    fn finalize(&mut self) -> Vec<u8> {
        self.hasher.finalize_reset().to_vec()
    }
}

fn format_hash(digest: Vec<u8>) -> String {
    digest
        .iter()
        .map(|&byte| format!("{:02x}", byte))
        .collect()
}


#[cfg(test)]
mod tests {
    use std::path::PathBuf;
    use crate::Hasher;
    use crate::format_hash;

    const BUFF_SIZE: usize = 256 * 1024 * 1024;

    #[test]
    fn hasher_test() {
        let mut hasher = Hasher::build("sha1", BUFF_SIZE).unwrap();

        hasher.update("".as_bytes());
        let actual = format_hash(hasher.finalize());
        let expected = "da39a3ee5e6b4b0d3255bfef95601890afd80709";
        assert_eq!(actual, expected);

        hasher.update("The quick brown fox jumps over the lazy dog".as_bytes());
        let actual = format_hash(hasher.finalize());
        let expected = "2fd4e1c67a2d28fced849ee1bb76e7391b93eb12";
        assert_eq!(actual, expected);
    }

    #[test]
    fn hash_file_test() {
        // Binary
        let file: PathBuf = PathBuf::from("test_files/test.bin");
        let mut hasher = Hasher::build("sha1", BUFF_SIZE).unwrap();
        let actual = hasher.hash_file(&file).unwrap();
        let expected = "8e1d7e501401059d1c632c0fb09fc37433953297";
        assert_eq!(actual, expected);

        let mut hasher = Hasher::build("sha256", BUFF_SIZE).unwrap();
        let actual = hasher.hash_file(&file).unwrap();
        let expected = "816842df6cb02fce31b6e591e709025c0b9a4560c5abd874dd3b45dd7d01aa84";
        assert_eq!(actual, expected);

        let mut hasher = Hasher::build("b2b", BUFF_SIZE).unwrap();
        let actual = hasher.hash_file(&file).unwrap();
        let expected = "fe9618f819065c3a7f1f16a2f57c8de8eb8a58d624fb342866738ae41ca48ff41ad4e83ff22b9a66486ad44de11136ba2d8aad6dcd4c30e26401b4f92fbc48de";
        assert_eq!(actual, expected);

        // Text
        let file = PathBuf::from("test_files/test.txt");
        let mut hasher = Hasher::build("sha1", BUFF_SIZE).unwrap();
        let actual = hasher.hash_file(&file).unwrap();
        let expected = "e15ed50daec5ca29661c13d5352e76f98a71cb1a";
        assert_eq!(actual, expected);

        let mut hasher = Hasher::build("sha256", BUFF_SIZE).unwrap();
        let actual = hasher.hash_file(&file).unwrap();
        let expected = "05d479a39df62500c192102eb8b3a4ff1204512aa123f2ea1651dded1669cecf";
        assert_eq!(actual, expected);

        let mut hasher = Hasher::build("b2b", BUFF_SIZE).unwrap();
        let actual = hasher.hash_file(&file).unwrap();
        let expected = "e2c1213f62b70c6c43a9b690693268079a9522bada03cc57caeee1c776c28ec7bbaaa3178d0df9aaabe33bd18d39d089bbf2990b4039701d3e274f39f13d3d5b";
        assert_eq!(actual, expected);
    }

    #[test]
    #[should_panic(expected = "HasherError(\"No such file")]
    fn hash_file_bad_test() {
        let file = PathBuf::from("thisdoesntexist.bin");

        let mut hasher = Hasher::build("sha256", BUFF_SIZE).unwrap();
        hasher.hash_file(&file).unwrap();
    }
}